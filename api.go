package tournies

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	"log"
	"net/http"
	"time"
	"tournies/data"
	"tournies/restr"
)

type App struct {
	Router HttpRouter
	Srv    *http.Server
	DB     *sqlx.DB
}

func (app *App) Start() {
	log.Printf("[ERROR] %s", app.Srv.ListenAndServe())
}

func (app *App) Stop() error {
	return app.Srv.Shutdown(context.Background())
}

func (app *App) clearDb() {
	app.DB.MustExec("TRUNCATE tournaments, games")
}

func NewApp(port int) (*App, error) {
	db, err := data.NewDbConnection()
	if err != nil {
		return nil, fmt.Errorf("[NewApp] error calling data.NewDbConnection(): %v", err)
	}

	mux, err := createRoutes(db)
	if err != nil {
		return nil, fmt.Errorf("[NewApp] error calling createRoutes(): %v", err)
	}
	addMiddlewares(mux)
	srv := &http.Server{
		Handler:      mux.Router,
		Addr:         fmt.Sprintf("127.0.0.1:%d", port),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	app := &App{
		Router: mux,
		Srv:    srv,
		DB:     db,
	}

	return app, nil
}

func createRoutes(db *sqlx.DB) (*GorillaRouter, error) {
	mux := NewGorillaRouter()

	tr, err := tournamentsResource(db)
	if err != nil {
		return nil, err
	}

	resources := make([]restr.Resource, 0)
	resources = append(resources, tr)

	for _, r := range resources {
		Add(r, mux)
	}

	return mux, nil
}

func addMiddlewares(mux HttpRouter) {
	mux.Use(loggingMiddleware)
}

func tournamentsResource(db *sqlx.DB) (*restr.TournamentResource, error) {
	dataSource, err := data.NewTournamentDataService(db)
	if err != nil {
		return nil, err
	}

	tr := &restr.TournamentResource{
		DataSource: dataSource,
	}

	return tr, nil
}
