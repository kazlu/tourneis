package data

import (
	"github.com/jmoiron/sqlx"
	"tournies/core"
)

type TournamentDataService interface {
	Save(core.NewTournament) (*core.Tournament, error)
	GetById(id int) (*core.Tournament, error)
	GetByName(name string) (*core.Tournament, error)
}

type PsqlTournamentDataService struct {
	db *sqlx.DB
}

func (ts *PsqlTournamentDataService) Save(newTournament core.NewTournament) (*core.Tournament, error) {
	tx := ts.db.MustBegin()
	_, err := tx.Exec("INSERT INTO tournaments (name, player_count) VALUES ($1, $2)",
		newTournament.Name, newTournament.PlayerCount)
	if err != nil {
		return nil, err
	}

	err = tx.Commit()
	if err != nil {
		return nil, err
	}

	return ts.GetByName(newTournament.Name)
}

func (ts *PsqlTournamentDataService) GetByName(name string) (*core.Tournament, error) {
	t := &core.Tournament{}
	err := ts.db.Get(t, "SELECT * FROM tournaments WHERE name=$1", name)
	if err != nil {
		return nil, err
	}

	return t, nil
}

func (ts *PsqlTournamentDataService) GetById(id int) (*core.Tournament, error) {
	t := &core.Tournament{}
	err := ts.db.Get(t, "SELECT * FROM tournaments WHERE id=$1", id)
	if err != nil {
		return nil, err
	}

	return t, nil
}

func NewTournamentDataService(db *sqlx.DB) (*PsqlTournamentDataService, error) {
	impl := &PsqlTournamentDataService{db: db}

	return impl, nil
}
