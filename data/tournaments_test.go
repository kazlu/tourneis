package data

import (
	"github.com/jmoiron/sqlx"
	"testing"
	"tournies/core"
)

func TestCreateQueryTournament(t *testing.T) {
	// Arrange
	const name = "jose"
	newTournament := core.NewTournament{
		Name:        name,
		PlayerCount: 5,
	}
	db, err := NewDbConnection()
	okClear(t, db, err)
	tourneyDataService, err := NewTournamentDataService(db)
	okClear(t, db, err)

	// Act
	tournament, err := tourneyDataService.Save(newTournament)
	okClear(t, db, err)
	fetched, err := tourneyDataService.GetByName(tournament.Name)
	okClear(t, db, err)

	// Assert
	if !tournament.Equals(fetched) {
		t.Errorf("Tournaments must be equal")
	}

	fetched, err = tourneyDataService.GetById(tournament.Id)
	okClear(t, db, err)

	if !tournament.Equals(fetched) {
		t.Errorf("Tournaments must be equal")
	}

	db.MustExec("DELETE from tournaments *")
}

func okClear(t *testing.T, db *sqlx.DB, err error) {
	if err != nil {
		db.MustExec("DELETE from tournaments *")
		t.Fatal(err)
	}
}
