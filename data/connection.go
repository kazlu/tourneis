package data

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"os"
)

func NewDbConnection() (*sqlx.DB, error) {
	const dbName = "tournies-dev"
	const extras = "sslmode=disable"

	dbHost := os.Getenv("TOURNEIS_DB_HOST")
	fmt.Printf("Connecting to host %s for db: %s\n", dbHost, dbName)

	dataSourceName := fmt.Sprintf("host=%s dbname=%s %s", dbHost, dbName, extras)
	db, err := sqlx.Connect("postgres", dataSourceName)
	if err != nil {
		return nil, fmt.Errorf("[NewDbConn] error calling sqlx.Connect %v", err)
	}

	return db, nil
}
