package core

import (
	"fmt"
	"testing"
)

func TestCreateTournament(t *testing.T) {
	for _, tt := range []struct {
		numberPlayers int
		// For each round how many games to expect
		expectedInRound []int
	}{
		{3, []int{2, 1}},
		{5, []int{4, 2, 1}},
		{8, []int{4, 2, 1}},
		{10, []int{8, 4, 2, 1}},
	} {
		testName := fmt.Sprintf("%d players -> %v games", tt.numberPlayers, tt.expectedInRound)
		t.Run(testName, func(t *testing.T) {
			// Arrange
			tc, err := NewTournamentCreator()
			ok(t, err)

			// Act
			tournament := tc.CreateTournament(tt.numberPlayers)

			// Assert
			totalGames := 0
			for idx, round := range tournament.Rounds {
				if len(round) != tt.expectedInRound[idx] {
					t.Errorf("Expected %d games got %d", tt.expectedInRound[idx], len(round))
				}

				totalGames += len(round)
				for _, game := range round {
					if game.Winner != "" {
						t.Errorf("Expected game to be unplayed")
					}
				}
			}
		})
	}
}

func TestCeilTwoPower(t *testing.T) {
	for _, tt := range []struct {
		in  int
		out int
	}{
		{2, 2},
		{3, 4},
		{4, 4},
		{5, 8},
		{7, 8},
		{8, 8},
		{10, 16},
		{14, 16},
	} {
		actual := CeilTwoPower(tt.in)
		if tt.out != actual {
			t.Errorf("Expected %d got %d", tt.out, actual)
		}
	}
}

func ok(t *testing.T, err error) {
	if err != nil {
		t.Errorf("%+v", err)
	}
}
