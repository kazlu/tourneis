package core

type NewTournament struct {
	Name        string `json:"name"`
	PlayerCount int    `json:"player-count"`
}

type Tournament struct {
	Id          int    `db:"id"`
	Name        string `db:"name"`
	PlayerCount int    `db:"player_count"`
	// Each round is a list of games
	Rounds [][]Game
}

func (t *Tournament) Equals(o *Tournament) bool {
	return t.Id == o.Id &&
		t.Name == o.Name &&
		t.PlayerCount == o.PlayerCount
}

type Game struct {
	// Every game other than the ones in the first round
	// depend on other games
	Id           int    `db:"id"`
	TournamentId int    `db:"tournament_id"`
	Round        int    `db:"round"`
	Winner       string `db:"winner"`
	Player1      string `db:"player1"`
	Player2      string `db:"player2"`

	// TODO add these to the DB
	Prev1Id int `db:"prev1_id"`
	Prev2Id int `db:"prev2_id"`

	Tournament   Tournament
	Prev1, Prev2 *Game
}

func (t *Game) Equals(o *Game) bool {
	return t.Id == o.Id &&
		t.TournamentId == o.TournamentId &&
		t.Round == o.Round &&
		t.Winner == o.Winner &&
		t.Player1 == o.Player1 &&
		t.Player2 == o.Player2
}
