package core

import "math"

type TournamentCreator struct{}

func (c *TournamentCreator) CreateTournament(nPlayers int) Tournament {
	matches := math.Ceil(float64(nPlayers) / 2)
	bracketsMatches := CeilTwoPower(int(matches))

	idx := 0
	rounds := make([][]Game, 0)
	for ; bracketsMatches > 1; {
		rounds = append(rounds, make([]Game, 0))
		for i := 0; i < bracketsMatches; i++ {
			g := Game{
				Winner:  "",
				Player1: "a",
				Player2: "b",
			}

			rounds[idx] = append(rounds[idx], g)
		}

		idx += 1
		bracketsMatches /= 2
	}

	g := Game{
		Winner:  "",
		Player1: "final1",
		Player2: "final2",
	}

	rounds = append(rounds, make([]Game, 0))
	rounds[idx] = append(rounds[idx], g)

	return Tournament{
		Rounds: rounds,
	}
}

func CeilTwoPower(n int) int {
	exp := uint(math.Ceil(math.Log2(float64(n))))
	return 1 << exp
}

func NewTournamentCreator() (*TournamentCreator, error) {
	t := &TournamentCreator{}
	return t, nil
}
