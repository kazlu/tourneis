ALTER TABLE games
  DROP COLUMN played CASCADE,
  DROP COLUMN result CASCADE,
  ADD COLUMN winner text,
  ADD COLUMN player1 text,
  ADD COLUMN player2 text;
