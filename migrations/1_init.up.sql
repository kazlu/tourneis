CREATE TABLE tournaments
(
  id   serial PRIMARY KEY,
  name text UNIQUE NOT NULL
);

CREATE TABLE games
(
  id            serial PRIMARY KEY,
  tournament_id integer NOT NULL,
  played        boolean NOT NULL DEFAULT false,
  result        text,
  FOREIGN KEY (tournament_id) REFERENCES tournaments (id)
);
