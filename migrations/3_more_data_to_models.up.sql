ALTER TABLE tournaments
  ADD COLUMN player_count int NOT NULL default 0;

ALTER TABLE games
  ADD COLUMN round int;
