ALTER TABLE games
  DROP CONSTRAINT prev1_game_id_fkey,
  DROP CONSTRAINT prev2_game_id_fkey,
  DROP COLUMN prev1_id CASCADE ,
  DROP COLUMN prev2_id CASCADE;

