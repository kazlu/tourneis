ALTER TABLE games
  ADD COLUMN prev1_id int,
  ADD COLUMN prev2_id int,
  ADD CONSTRAINT prev1_game_id_fkey FOREIGN KEY (prev1_id) REFERENCES games (id),
  ADD CONSTRAINT prev2_game_id_fkey FOREIGN KEY (prev2_id) REFERENCES games (id);

