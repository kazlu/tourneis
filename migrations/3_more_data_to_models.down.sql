ALTER TABLE tournaments
  DROP COLUMN player_count CASCADE;

ALTER TABLE games
  DROP COLUMN round CASCADE;
