package tournies

import (
	"bytes"
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
	"tournies/restr"
)

type HttpRouter interface {
	// Adds the url with the given method to the Router
	Add(url string, method string, fn func(http.ResponseWriter, *http.Request))

	// Use the given middleware in the router
	Use(middleware func(http.Handler) http.Handler)
}

type GorillaRouter struct {
	Router *mux.Router
}

func (r *GorillaRouter) Add(url string, method string, fn func(http.ResponseWriter, *http.Request)) {
	r.Router.HandleFunc(url, fn).Methods(method)
}

func (r *GorillaRouter) Use(middleware func(http.Handler) http.Handler) {
	r.Router.Use(middleware)
}

func NewGorillaRouter() *GorillaRouter {
	router := mux.NewRouter()
	return &GorillaRouter{
		Router: router,
	}
}

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Printf("[ERROR] %s", err)
			panic(err)
		}

		err = r.Body.Close()
		if err != nil {
			log.Printf("[ERROR] closing body %s", err)
			panic(err)
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(body))
		requestInfo := fmt.Sprintf("%s %s\n%s", r.Method, r.URL,
			string(body))
		log.Printf("[INFO] %s", requestInfo)
		next.ServeHTTP(w, r)
	})
}

func Add(resource restr.Resource, router HttpRouter) {
	handleGet := func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		result, err := resource.Get(vars["id"])
		if err != nil {
			writeError(err, w)
		}

		_, err = w.Write([]byte(result))
		if err != nil {
			log.Printf("[ERROR] %s", err)
		}
	}

	handlePost := func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			writeError(err, w)
		}

		result, err := resource.New(body)
		if err != nil {
			writeError(err, w)
		}

		_, err = w.Write([]byte(result))
		if err != nil {
			log.Printf("[ERROR] %s", err)
		}
	}

	urlGet := fmt.Sprintf("/%s/{id:[0-9]+}", resource.Url())
	urlPost := fmt.Sprintf("/%s", resource.Url())

	router.Add(urlGet, "GET", handleGet)
	router.Add(urlPost, "POST", handlePost)
}

func writeError(err error, w http.ResponseWriter) {
	w.WriteHeader(400)
	_, _ = w.Write([]byte(err.Error()))
	log.Printf("[ERROR] %s", err.Error())
}

