package restr

import (
	"encoding/json"
	"strconv"
	"tournies/core"
	"tournies/data"
)

type TournamentResource struct {
	DataSource data.TournamentDataService
}

func (t *TournamentResource) Url() string {
	return "tournaments"
}

func (t *TournamentResource) New(jsonData []byte) ([]byte, error) {
	var newData core.NewTournament
	err := json.Unmarshal(jsonData, &newData)
	if err != nil {
		return nil, err
	}

	tournament, err := t.DataSource.Save(newData)
	if err != nil {
		return nil, err
	}

	rawJson, err := json.Marshal(tournament)
	if err != nil {
		return nil, err
	}

	return rawJson, nil
}

func (t *TournamentResource) Get(id string) ([]byte, error) {
	idInt, err := strconv.Atoi(id)
	if err != nil {
		return nil, err
	}
	tournament, err := t.DataSource.GetById(idInt)
	if err != nil {
		return nil, err
	}

	rawJson, err := json.Marshal(tournament)
	if err != nil {
		return nil, err
	}

	return rawJson, nil
}
