package restr

// Simple resource interface to represent restr in a
// REST API.
type Resource interface {
	Url() string
	New(jsonData []byte) ([]byte, error)
	Get(id string) ([]byte, error)
}
