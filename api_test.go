package tournies

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"
	"testing"
	"tournies/core"
)

func TestCreateTournament(t *testing.T) {
	// Arrange
	var wg sync.WaitGroup
	const port = 8085
	app, err := NewApp(port)
	ok(t, err)

	app.clearDb()

	wg.Add(1)
	go func() {
		defer wg.Done()
		app.Start()
	}()

	t.Run("new and get", func(t *testing.T) {
		payload := createPayload(`{"name": "nicegame", "player-count": 4}`)
		url := fmt.Sprintf("http://localhost:%d/tournaments", port)

		// Act
		res, err := http.Post(url, "application/json", payload)
		ok(t, err)

		// Assert
		checkStatusCode(t, res, http.StatusOK)
		rawContent, err := ioutil.ReadAll(res.Body)
		ok(t, err)
		var tournament core.Tournament
		err = json.Unmarshal(rawContent, &tournament)
		ok(t, err)

		if tournament.Name != "nicegame" {
			t.Errorf("Expected nicegame got %s", tournament.Name)
		}

		if tournament.PlayerCount != 4 {
			t.Errorf("Expected 4 got %d", tournament.PlayerCount)
		}

		//Cleanup
		err = app.Stop()
		ok(t, err)
	})

	wg.Wait()
}

func checkStatusCode(t *testing.T, res *http.Response, expectedStatus int) {
	if res.StatusCode != expectedStatus {
		t.Errorf("Expected %d got %d", expectedStatus, res.StatusCode)
	}
}

func createPayload(s string) *bytes.Buffer {
	return bytes.NewBuffer([]byte(s))
}

func ok(t *testing.T, err error) {
	if err != nil {
		t.Fatalf("%v", err)
	}
}
