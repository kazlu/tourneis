package main

import (
	"log"
	"tournies"
)

func main() {
	const port = 8090
	app, err := tournies.NewApp(port)
	log.Printf("[INFO] running in port %d", port)
	if err != nil {
		panic(err)
	}

	app.Start()
}
