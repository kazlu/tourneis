package tournies

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

type MockHttpRouter struct {
	rec map[string][]string
}

func NewMockRouter() *MockHttpRouter {
	rec := make(map[string][]string)
	return &MockHttpRouter{rec: rec}
}

func (r *MockHttpRouter) Add(url string, method string, fn func(http.ResponseWriter, *http.Request)) {
	_, ok := r.rec[url]
	if !ok {
		r.rec[url] = make([]string, 0)
	}

	r.rec[url] = append(r.rec[url], method)
}

func (r *MockHttpRouter) Use(middleware func(http.Handler) http.Handler) {}

func (r *MockHttpRouter) checkAdded(url string, method string) bool {
	methods, ok := r.rec[url]
	if !ok {
		return false
	}

	return contains(methods, method)
}

type verifyCall bool

func (c *verifyCall) call(w http.ResponseWriter, r *http.Request) {
	*c = true
}

func TestRouteAdded(t *testing.T) {
	// Arrange
	var called verifyCall = false
	r := NewGorillaRouter()

	// Act
	r.Add("/one", "POST", called.call)
	res := executeRequest(r, "POST", "/one", nil)

	// Assert
	assert(res.Code == http.StatusOK, fmt.Sprintf("Expected 200 got %d", res.Code), t)
	assert(bool(called), "Expected called to be true", t)
}

type PageResource struct{}

func (PageResource) Url() string {
	return "pages"
}

func (PageResource) New(jsonData []byte) ([]byte, error) {
	return jsonData, nil
}

func (PageResource) Get(id string) ([]byte, error) {
	return []byte(fmt.Sprintf("id:%s", id)), nil
}

func TestAddToRouter(t *testing.T) {
	// Arrange
	router := NewMockRouter()
	myResource := PageResource{}

	// Act
	Add(myResource, router)

	// Assert
	assert(router.checkAdded("/pages/{id:[0-9]+}", "GET"), "no GET /pages ", t)
	assert(router.checkAdded("/pages", "POST"), "no POST /pages ", t)
}

func assert(expr bool, msg string, t *testing.T) {
	if !expr {
		t.Error(msg)
	}
}

func executeRequest(router *GorillaRouter, method string, url string, body io.Reader) *httptest.ResponseRecorder {
	req := httptest.NewRequest(method, url, body)
	res := httptest.NewRecorder()
	router.Router.ServeHTTP(res, req)

	return res
}

// Returns true if a contains b along with the index
// If false returns -1
func contains(a []string, b string) bool {
	for _, elt := range a {
		if elt == b {
			return true
		}
	}

	return false
}
